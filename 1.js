//Опишите своими словами, что такое метод обьекта
//Метод объекта – это функция, которая является значением свойства объекта и,
// следовательно, задачей, которую может выполнять объект. Методы обьекта выражаются глаголами.

// Опишите своими словами, что такое экранирование, и зачем оно нужно в языках программирования.
//Экранирование символов означает, что мы делаем что-то с ними, чтобы убедиться, что они распознаются как текст, а не часть кода.
// В JS мы делаем это, размещая обратный слэш перед символом.



function createNewUser() {
  const user = {};
  const firstName = prompt('Enter your name');
  const lastName = prompt('Enter your lastname');
  const birthday = prompt('Enter your date of birth', 'dd.mm.yyyy');

  user.firstName = firstName;
  user.lastName = lastName;
  user.birthday = birthday;
  user.getLogin = function () {
    const firstLetter = this.firstName.charAt(0);
    console.log((firstLetter + this.lastName).toLowerCase());
  };

  user.getAge = function () {
    const date = new Date();
    const currentYear = date.getFullYear();
    const birthYear = this.birthday.substr(this.birthday.length - 4);
    console.log(currentYear - birthYear);
  };

  user.getPassword = function () {
    const firstLetter = this.firstName.charAt(0).toUpperCase();
    const surname = this.lastName.toLowerCase();
    const year = this.birthday.substr(this.birthday.length - 4);
    console.log(firstLetter + surname + year);
  };

  return Object.freeze(user);
}

const newUser = createNewUser();
newUser.getLogin();
newUser.getAge();
newUser.getPassword();


